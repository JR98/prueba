package com.pruebadeingreso.dao

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.pruebadeingreso.models.Users
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DataBaseTest: TestCase() {

    private lateinit var db: DataBase
    private lateinit var dao: UsersDao

    @Before
    public override fun setUp() {

        val context = ApplicationProvider.getApplicationContext<Context>()
        val db = Room.inMemoryDatabaseBuilder(context, DataBase::class.java).build()
        dao = db.usersDao()

    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun writeAndReadUser() = runBlocking {

        val obj = Users(1, "David Hoyos","3214543054","davidh@gmail.com")
        val user = listOf(obj)

        dao.insert(user)
        val users = dao.getAll()

        assertTrue(users.contains(obj))

    }
}