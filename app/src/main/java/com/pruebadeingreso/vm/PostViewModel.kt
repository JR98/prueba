package com.pruebadeingreso.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.pruebadeingreso.api.repositories.PostRepository
import com.pruebadeingreso.container.App
import com.pruebadeingreso.models.BackRes
import kotlinx.coroutines.launch

class PostViewModel(private val postRepository: PostRepository) : ViewModel() {

    private val dbP = App.instanceDb.postsDao()
    private val _info = MutableLiveData<BackRes>()
    val info: LiveData<BackRes> get() = _info

    fun fetchPosts(connect: Boolean, userId: Int) = viewModelScope.launch {
        val response = if (connect)  {

            if (dbP.getAll().isEmpty() || dbP.findById(userId).isEmpty()) postRepository.doGetUsers(dbP, userId)
            else BackRes(Gson().toJson(dbP.findById(userId)))

        } else BackRes(Gson().toJson(dbP.findById(userId)))

        _info.value = response
    }
}