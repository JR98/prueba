package com.pruebadeingreso.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.pruebadeingreso.api.repositories.UserRepository
import com.pruebadeingreso.container.App
import com.pruebadeingreso.models.BackRes
import kotlinx.coroutines.launch

class UserViewModel(private val userRepository: UserRepository) : ViewModel() {

    private val dbU = App.instanceDb.usersDao()
    private val _info = MutableLiveData<BackRes>()
    val info: LiveData<BackRes> get() = _info

    fun fetchUsers(connect: Boolean) = viewModelScope.launch {
        val response = if (connect)  {

            if (dbU.getAll().isEmpty()) userRepository.doGetUsers(dbU)
            else BackRes(Gson().toJson(dbU.getAll()))

        } else BackRes(Gson().toJson(dbU.getAll()))

        _info.value = response
    }
}