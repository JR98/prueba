package com.pruebadeingreso.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Users(
    @PrimaryKey(autoGenerate = true) var id: Int,

    var name: String,

    var phone: String,

    var email: String
)