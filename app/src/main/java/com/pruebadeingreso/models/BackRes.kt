package com.pruebadeingreso.models

import okhttp3.ResponseBody

class BackRes {

    constructor(list: String){
        this.list = list
    }

    constructor(list: String, error: ResponseBody?){
        this.list = list
        this.error = error
    }

    var list: String? = null
    var error: ResponseBody? = null
}