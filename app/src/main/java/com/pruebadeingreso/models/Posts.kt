package com.pruebadeingreso.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Posts(
    @PrimaryKey(autoGenerate = true) var id: Int,

    var title: String,

    var body: String,

    var userId: Int
)