package com.pruebadeingreso.di.modules

import com.pruebadeingreso.vm.PostViewModel
import com.pruebadeingreso.vm.UserViewModel
import org.koin.dsl.module

val modules = module {

    factory { UserViewModel( get() ) }

    factory { PostViewModel( get() ) }

}