package com.pruebadeingreso.di.modules

import com.pruebadeingreso.api.repositories.PostRepository
import com.pruebadeingreso.api.repositories.UserRepository
import org.koin.dsl.module

val repositoriesModule = module {

    factory { UserRepository( get() ) }

    factory { PostRepository( get() ) }

}