package com.pruebadeingreso.api.repositories

import com.google.gson.Gson
import com.pruebadeingreso.api.ApiInterface
import com.pruebadeingreso.dao.UsersDao
import com.pruebadeingreso.models.BackRes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepository(private val apiInterface: ApiInterface) {

    suspend fun doGetUsers(daoUs: UsersDao) = withContext(Dispatchers.IO) {
        try {
            val response = apiInterface.doGetListResources()

            if (response.isSuccessful) {

                val success = response.body()!!
                daoUs.insert(success)
                BackRes(Gson().toJson(success))

            } else {
                BackRes(Gson().toJson(daoUs.getAll()), response.errorBody())
            }
        } catch (e: KotlinNullPointerException) {
            e.printStackTrace()
            BackRes(Gson().toJson(daoUs.getAll()))
        }
    }

}