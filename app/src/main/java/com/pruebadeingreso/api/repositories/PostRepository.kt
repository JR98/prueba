package com.pruebadeingreso.api.repositories

import com.google.gson.Gson
import com.pruebadeingreso.api.ApiInterface
import com.pruebadeingreso.dao.PostsDao
import com.pruebadeingreso.dao.UsersDao
import com.pruebadeingreso.models.BackRes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PostRepository(private val apiInterface: ApiInterface) {

    suspend fun doGetUsers(daoPs: PostsDao, userId: Int) = withContext(Dispatchers.IO) {
        try {
            val response = apiInterface.doGetListPost(userId)

            if (response.isSuccessful) {

                val success = response.body()!!
                daoPs.insert(success)
                BackRes(Gson().toJson(success))

            } else {
                BackRes(Gson().toJson(daoPs.getAll()), response.errorBody())
            }
        } catch (e: KotlinNullPointerException) {
            e.printStackTrace()
            BackRes(Gson().toJson(daoPs.getAll()))
        }
    }

}