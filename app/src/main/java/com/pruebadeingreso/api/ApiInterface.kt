package com.pruebadeingreso.api

import com.pruebadeingreso.models.Posts
import com.pruebadeingreso.models.Users
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("users")
    suspend fun doGetListResources(): Response<List<Users>?>

    @GET("posts?")
    suspend fun doGetListPost(@Query("userId") userId: Int): Response<List<Posts>?>

}