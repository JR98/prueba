package com.pruebadeingreso.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pruebadeingreso.R
import com.pruebadeingreso.models.Posts

class PostAdapter(private val box: List<Posts>, private val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.post_list_item, parent, false)
        val vh: RecyclerView.ViewHolder?
        vh = LinearViewHolder(v)
        return vh
    }

    override fun getItemCount(): Int = box.size

    class LinearViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvTitle = view.findViewById<TextView>(R.id.title)
        internal var tvBody = view.findViewById<TextView>(R.id.body)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LinearViewHolder){

            holder.tvTitle.text = box[position].title
            holder.tvBody.text = box[position].body

        }
    }
}