package com.pruebadeingreso.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Filter
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.pruebadeingreso.view.PostActivity
import com.pruebadeingreso.R
import com.pruebadeingreso.container.App.Companion.buttonClick
import com.pruebadeingreso.models.Users

open class UserAdapter(private val box: List<Users>, private val context: Context,
                       private val emptyView: View) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.user_list_item, parent, false)
        val vh: RecyclerView.ViewHolder?
        vh = LinearViewHolder(v)
        return vh
    }

    override fun getItemCount(): Int = listFilter!!.size

    class LinearViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal var tvName = view.findViewById<TextView>(R.id.name)
        internal var tvPhone = view.findViewById<TextView>(R.id.phone)
        internal var tvEmail = view.findViewById<TextView>(R.id.email)
        internal var btnToPost = view.findViewById<Button>(R.id.toPost)
    }

    private var listFilter: List<Users>? = null

    init {
        this.listFilter = box
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LinearViewHolder){

            holder.tvName.text = listFilter!![position].name
            holder.tvPhone.text = listFilter!![position].phone
            holder.tvEmail.text = listFilter!![position].email

            holder.btnToPost.setOnClickListener {
                it.startAnimation(buttonClick)

                val obj = Gson().toJson(listFilter!![position])
                context.startActivity( Intent(context, PostActivity::class.java).putExtra("user", obj))
            }
        }
    }

    fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                listFilter = results.values as List<Users>
                for (item in listFilter as List<Users>){
                }

                emptyView.visibility = if (listFilter.
                    isNullOrEmpty()) View.VISIBLE else View.GONE

                notifyDataSetChanged()
            }

            override fun performFiltering(constraint: CharSequence): FilterResults {
                val filteredResults: List<Users>
                if (constraint.isEmpty()) {
                    filteredResults = box
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase())
                }

                val results = FilterResults()
                results.values = filteredResults

                return results
            }
        }
    }

    protected fun getFilteredResults(constraint: String): ArrayList<Users> {
        val results = ArrayList<Users>()

        for (item in box) {
            if (item.name.toLowerCase().contains(constraint.toLowerCase()) ||
                item.phone.toLowerCase().contains(constraint.toLowerCase()) ||
                item.email.toLowerCase().contains(constraint.toLowerCase())) {
                results.add(item)
            }
        }
        return results
    }
}