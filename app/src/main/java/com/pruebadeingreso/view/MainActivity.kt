package com.pruebadeingreso.view

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.pruebadeingreso.adapter.UserAdapter
import com.pruebadeingreso.container.KComponent
import com.pruebadeingreso.models.Users
import com.google.gson.reflect.TypeToken
import com.pruebadeingreso.R


class MainActivity : AppCompatActivity() {

    private lateinit var linear: LinearLayoutManager
    private lateinit var recyclerView: XRecyclerView
    private var userAdapter: UserAdapter? = null

    private lateinit var contextView: View
    private val kComponent = KComponent()
    lateinit var emptyView: View
    lateinit var progressBar: ProgressBar

    private val delay: Long = 1000
    private var editTextLastWord: Long = 0
    internal var handler = Handler()
    private var searchView: SearchView? = null

    private var search = ""

    private val runnable = Runnable {
        if (System.currentTimeMillis() > editTextLastWord + delay - 500) {
            userAdapter!!.getFilter().filter(search)
            progressBar.visibility = View.GONE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contextView = findViewById(R.id.main_view)
        emptyView = findViewById(R.id.emptyView)
        progressBar = findViewById(R.id.userLoading)
        searchView = findViewById(R.id.searchUsers)

        //instance recyclerView & gridLayout
        recyclerView = findViewById(R.id.xReUsers)
        linear = LinearLayoutManager(this)
        recyclerView.layoutManager = linear
        recyclerView.setHasFixedSize(true)

        recyclerView.setPullRefreshEnabled(false)
        recyclerView.setLoadingMoreEnabled(false)

        //retrofit and koin with coroutines
        kComponent.userViewModel.info.observe(this, { data ->
            if (data.list!!.isNotBlank()) {

                val listType = object : TypeToken<List<Users>>() {}.type
                val listUser = Gson().fromJson<List<Users>>(data.list!!, listType)
                addAdap(listUser)

                emptyView.visibility = View.GONE
                searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(s: String): Boolean {
                        userAdapter!!.getFilter().filter(s)
                        return false
                    }

                    override fun onQueryTextChange(s: String): Boolean {
                        progressBar.visibility = View.VISIBLE
                        search = s
                        handler.removeCallbacks(runnable)

                        editTextLastWord = System.currentTimeMillis()
                        handler.postDelayed(runnable, delay)
                        return false
                    }
                })

            } else emptyView.visibility = View.VISIBLE

            progressBar.visibility = View.GONE
        })

        loadUsers()

        emptyView.setOnClickListener {
            if (search.isEmpty()) loadUsers()
        }

    }

    private fun addAdap(users: List<Users>) {
        userAdapter = UserAdapter(users, this, emptyView)
        recyclerView.adapter = userAdapter!!
    }

    private fun loadUsers() {
        if (!isNetworkAvailable()) snackB()
        progressBar.visibility = View.VISIBLE
        kComponent.userViewModel.fetchUsers(isNetworkAvailable())
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    private fun snackB() {
        Snackbar.make(contextView, getString(R.string.snackBarErr), Snackbar.LENGTH_SHORT).show()
    }
}