package com.pruebadeingreso.view

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.pruebadeingreso.R
import com.pruebadeingreso.adapter.PostAdapter
import com.pruebadeingreso.container.KComponent
import com.pruebadeingreso.models.Posts
import com.pruebadeingreso.models.Users

class PostActivity : AppCompatActivity() {

    private lateinit var linear: LinearLayoutManager
    private lateinit var recyclerView: XRecyclerView
    private var postAdapter: PostAdapter? = null

    private lateinit var contextView: View
    private val kComponent = KComponent()
    lateinit var emptyView: View
    lateinit var progressBar: ProgressBar

    lateinit var tvname: TextView
    lateinit var tvphone: TextView
    lateinit var tvemail: TextView

    private var user: Users? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        val toolbar: Toolbar = findViewById(R.id.toolbarPost)
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_ios_24)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        contextView = findViewById(R.id.post_view)
        emptyView = findViewById(R.id.emptyPs)
        progressBar = findViewById(R.id.postLoading)

        tvname = findViewById(R.id.pName)
        tvphone = findViewById(R.id.pPhone)
        tvemail = findViewById(R.id.pEmail)

        user = Gson().fromJson(intent.extras!!.getString("user"), Users::class.java)

        tvname.text = user!!.name
        tvphone.text = user!!.phone
        tvemail.text = user!!.email

        //instance recyclerView & gridLayout
        recyclerView = findViewById(R.id.xRePosts)
        linear = LinearLayoutManager(this)
        recyclerView.layoutManager = linear
        recyclerView.setHasFixedSize(true)

        recyclerView.setPullRefreshEnabled(false)
        recyclerView.setLoadingMoreEnabled(false)

        //retrofit and koin with coroutines
        kComponent.postViewModel.info.observe(this, { data ->
            if (data.list!!.length > 2) {

                val listType = object : TypeToken<List<Posts>>() {}.type
                val listPost = Gson().fromJson<List<Posts>>(data.list!!, listType)
                addAdap(listPost)

                emptyView.visibility = View.GONE

            } else emptyView.visibility = View.VISIBLE

            progressBar.visibility = View.GONE
        })

        loadUsers()

        emptyView.setOnClickListener {
            loadUsers()
        }
    }

    private fun addAdap(posts: List<Posts>) {
        postAdapter = PostAdapter(posts, this)
        recyclerView.adapter = postAdapter!!
    }

    private fun loadUsers() {
        if (!isNetworkAvailable()) snackB()
        progressBar.visibility = View.VISIBLE
        kComponent.postViewModel.fetchPosts(isNetworkAvailable(), user!!.id)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    private fun snackB() {
        Snackbar.make(contextView, getString(R.string.snackBarErr), Snackbar.LENGTH_SHORT).show()
    }
}