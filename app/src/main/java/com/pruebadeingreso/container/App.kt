package com.pruebadeingreso.container

import android.app.Application
import android.view.animation.AlphaAnimation
import com.pruebadeingreso.dao.DataBase
import com.pruebadeingreso.di.modules.modules
import com.pruebadeingreso.di.modules.repositoriesModule
import com.pruebadeingreso.di.retrofitModule
import org.koin.core.context.startKoin

class App: Application() {

    private val appModule = listOf( retrofitModule, repositoriesModule, modules )

    override fun onCreate() {
        super.onCreate()

        instanceDb = DataBase.getInstance(this)

        startKoin {
            modules(appModule)
        }
    }

    companion object {
        const val apiUrl = "https://jsonplaceholder.typicode.com/"
        val buttonClick = AlphaAnimation(1f, 0.6f)

        @get:Synchronized
        lateinit var instanceDb: DataBase
    }
}