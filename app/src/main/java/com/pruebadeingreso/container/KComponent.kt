package com.pruebadeingreso.container

import com.pruebadeingreso.vm.PostViewModel
import com.pruebadeingreso.vm.UserViewModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class KComponent: KoinComponent {

    val userViewModel: UserViewModel by inject()

    val postViewModel: PostViewModel by inject()

}