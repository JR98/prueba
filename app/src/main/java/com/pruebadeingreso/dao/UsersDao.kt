package com.pruebadeingreso.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.pruebadeingreso.models.Users

@Dao
interface UsersDao {

    @Query("SELECT * FROM Users")
    fun getAll(): List<Users>

    @Insert
    fun insert(users: List<Users>)

    @Query("DELETE FROM Users")
    fun deleteAll()

    /*@Query("SELECT * FROM Users WHERE id = :id")
    fun findById(id: Int): Users

    @Update
    fun update(users: Users)

    @Delete
    fun delete(users: Users)*/
}