package com.pruebadeingreso.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.pruebadeingreso.models.Posts
import com.pruebadeingreso.models.Users

@Dao
interface PostsDao {

    @Query("SELECT * FROM Posts")
    fun getAll(): List<Posts>

    @Insert
    fun insert(posts: List<Posts>)

    @Query("SELECT * FROM Posts WHERE userId = :id")
    fun findById(id: Int): List<Posts>
}