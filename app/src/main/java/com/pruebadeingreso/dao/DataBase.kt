package com.pruebadeingreso.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.pruebadeingreso.models.Posts
import com.pruebadeingreso.models.Users

@Database(entities = [Users::class, Posts::class], version = 1)
abstract class DataBase: RoomDatabase() {

    abstract fun usersDao(): UsersDao
    abstract fun postsDao(): PostsDao

    companion object {

        private var INSTANCE: DataBase? = null

        fun getInstance(context: Context): DataBase {
            if (INSTANCE == null) {

                INSTANCE = Room.databaseBuilder(
                    context,
                    DataBase::class.java,
                    "test-db")
                    .allowMainThreadQueries()
                    .build()
            }

            return INSTANCE!!
        }
    }
}